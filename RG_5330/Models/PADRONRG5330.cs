﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RG_5330.Models
{
    class PADRONRG5330
    {
        public int COD_ADQUIRENTE { get; set; }
        public string ORIGEN { get; set; }
        public int VIGENCIA { get; set; }
        public Int64 CUIT_ESTABLECI { get; set; }
        public string NUM_ESTABLECI { get; set; }
        public int BIN_ADQUIRENTE { get; set; }
        public int MARCA_CODI { get; set; }
    }
}
