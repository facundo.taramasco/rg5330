﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RG_5330.Models
{
    class ACUMULARG5330
    {
		public int MARCACODI { get; set; }
		public int SOLINUME { get; set; }
		public DateTime PRESEFECHA { get; set; }
		public decimal CONSUIMPOR { get; set; }
		public decimal IMPORPERCI { get; set; }
		public decimal IMPORTOTALPERCI { get; set; }
		public int PAGAOTORENTICODI { get; set; }
		public string COMERISOID { get; set; }
		public string COMERISODESCRI { get; set; }
		public long CONSUCUPON { get; set; }
		public int AJUSCONCEPCODI { get; set; }
        public string PERSODOCUNUMEOTRO { get; set; }
    }
}
