﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using System.IO;
using System.Configuration;
using System.Globalization;
using System.Threading.Tasks;
using RG_5330.AccesoDatos;
using RG_5330.Models;

namespace RG_5330
{
    class Proceso
    {

        private static string GetCadenaConexion()
        {
            var descifrado = File.ReadAllBytes(ConfigurationManager.AppSettings["conexionDB"]);
            var encoding = new UTF8Encoding();
            var crypto = new Crypt3Des.CTripleDESUtil();
            return crypto.DesEncriptar(descifrado);
            //return "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=10.87.2.67)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=GPINFUAT))); User Id=Globalprod;Password=AndaGp.20;";
        }

        private static string ArmarNombreLP(string idMarca, string idEntidad)
        {
            string fechaarc = DateTime.Now.ToString("yyyyMMdd_HHmm");
            return "R3021D_" + idMarca.PadLeft(2, '0') + "_" + idEntidad.PadLeft(3, '0') + "_" + fechaarc + ".txt";
        }

        public static void GenRG5330XEntidad(string rutaSalida, string fecha_desde, string fecha_hasta, int marca, int entidad, Logger logger)
        {
            var cadenaConexion = GetCadenaConexion();
            try
            {
                DateTime dt = DateTime.Parse(fecha_desde, new CultureInfo("es-AR"));

                string mes = dt.Month.ToString();
                string anio = dt.Year.ToString();

                decimal montotopeacum = Convert.ToDecimal(ConfigurationManager.AppSettings["montotopeacum"]);
                int reintegroporcen = Convert.ToInt32(ConfigurationManager.AppSettings["reintegroporcen"]);

                using (var baseDatos = new BaseDatos(cadenaConexion, logger))
                {
                    //var listaEntiProduc = GetListaEntiProduc();
                    //string entidades = GetEntidades(listaEntiProduc);
                    //string productos = GetProductos(listaEntiProduc);

                    //string entidad = ConfigurationManager.AppSettings["Entidad"];
                    //string producto = ConfigurationManager.AppSettings["Producto"];

                    //Lista en la tabla Comercio
                    logger.Info("Vamos por consumos");
                    List<RG5330> lst_consumos = baseDatos.GetDatosConsumos(fecha_desde, fecha_hasta, entidad, marca);
                    ACUMULARG5330 acumula = new ACUMULARG5330();
                    logger.Info("Ya cargue la lista de ConsumosRG5330");
                    string vigencia = DateTime.Now.ToString("yyyyMM");

                    logger.Info("ingreso al FOR()");
                    logger.Info("## Cantidad de registros = " + lst_consumos.Count);
                    for (int i = 0; i < lst_consumos.Count; i++)
                    {
                        //---------------------------------------------------------------------//
                        // Calcula cuanto viene acumulando
                        //---------------------------------------------------------------------//
                        decimal acumulado = 0;
                        baseDatos.GetSumaAcum(lst_consumos[i].MARCACODI, lst_consumos[i].PERSODOCUNUMEOTRO, mes, anio, ref acumulado);

                        logger.Info("Paso por el acumulador");
                        //---------------------------------------------------------------------//
                        // Acumula en la tabla
                        //---------------------------------------------------------------------//
                        acumula.MARCACODI = lst_consumos[i].MARCACODI;
                        acumula.SOLINUME = lst_consumos[i].SOLINUME;
                        acumula.PERSODOCUNUMEOTRO = lst_consumos[i].PERSODOCUNUMEOTRO;
                        acumula.PRESEFECHA = lst_consumos[i].AUTOFECHA;
                        acumula.IMPORPERCI = lst_consumos[i].CONSUIMPOR * 10 / 100;
                        if (lst_consumos[i].MOVIMCODI == 801)
                        {
                            acumula.IMPORTOTALPERCI = acumulado - acumula.IMPORPERCI; //Restar movim creditos (ajus=debito)
                        }
                        else
                        {
                            acumula.IMPORTOTALPERCI = acumulado + acumula.IMPORPERCI; //Restar movim debitos (ajus=credito)
                        }
                        acumula.PAGAOTORENTICODI = lst_consumos[i].PAGAOTORENTICODI;
                        acumula.COMERISOID = lst_consumos[i].CONSUCOMERISOID;
                        acumula.COMERISODESCRI = lst_consumos[i].CONSUCOMERISODESCRI;
                        acumula.CONSUCUPON = lst_consumos[i].CONSUCUPON;

                        logger.Info("Paso por los ajustes");
                        //---------------------------------------------------------------------//
                        // Ajustes en la tabla
                        //---------------------------------------------------------------------//
                        AJUSTESRG5330 ajus = new AJUSTESRG5330();
                        ajus.PAGAOTORENTICODI = lst_consumos[i].PAGAOTORENTICODI;
                        ajus.SUCURENTICODI = 10; //verificar la sucursal
                        ajus.COBRAENTICODI = 10; //verificar entidad cobradora
                        ajus.SUCURCOBRAENTICODI = 10; //verificar sucursal de la entidad cobradora
                        ajus.MONECODI = lst_consumos[i].MONECODI;
                        ajus.COMPROBANTE = baseDatos.GetNumAjus(); //verificar el comprobante => guid?
                        ajus.MOVIMFECHA = lst_consumos[i].AUTOFECHA;
                        ajus.SOLINUME = lst_consumos[i].SOLINUME;
                        ajus.IMPORAJUS = lst_consumos[i].AUTOIMPORTOTAL * 10 / 100;
                        ajus.CANTCUOTAS = 1; //validar
                        //ajus.CONSUFECHA = lst_consumos[i].AUTOFECHA;
                        ajus.IMPORCONSU = lst_consumos[i].AUTOIMPORTOTAL;
                        ajus.MCC = lst_consumos[i].MCC;
                        ajus.CONSUICA = lst_consumos[i].CONSUICA;
                        ajus.AUTOIMPORTOTAL = lst_consumos[i].AUTOIMPORTOTAL;
                        ajus.PERSODOCUNUMEOTRO = lst_consumos[i].PERSODOCUNUMEOTRO;

                        //if (acumula.IMPORTOTALPERCI < montotopeacum) //REVISAR PORQUE PASA 1 TRX MAS
                        //{
                        //Generamos el ajuste
                        if (lst_consumos[i].MOVIMCODI == 501) //Consumos
                        {
                            //Código Ajuste: 696 | Descripción: REINTEGRO CARNICERIAS MINORISTAS
                            acumula.AJUSCONCEPCODI = 696;
                            ajus.DEBCRED = 1; //1= credito
                            ajus.CONCEPTAJUS = 696;
                        }
                        if (lst_consumos[i].MOVIMCODI == 801) // Devolucion consumos
                        {
                            //Código Ajuste: 695 | Descripción: DEV REINTEGRO CARNICERIAS MINORISTAS
                            acumula.AJUSCONCEPCODI = 695;
                            ajus.DEBCRED = 2; //2=debito
                            ajus.CONCEPTAJUS = 695;
                        }

                        //Inserta datos en acumula
                        //logger.Info("Inserto datos en Acumulador");
                        baseDatos.InsertAcumula(acumula);
                        //Inserta datos en ajustes
                        //logger.Info("Inserto datos en ajuste");
                        baseDatos.InsertAjusta(ajus);
                        logger.Info("## Inicio el proceso - N° " + i);
                        //}
                    }
                    logger.Info("llamo a la funcion que actualiza los consumos procesados (CONSUMOSRG5330)");

                    baseDatos.ActualizaConsumosRG5330();
                    logger.Info("Armo el R3021D");
                    GenerarR3021D(logger, baseDatos, rutaSalida, fecha_desde, fecha_hasta);
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                if (ex.InnerException != null)
                {
                    logger.Info(ex.InnerException.Message + ": " + ex.StackTrace);
                }
            }
        }

        #region Reporte R3021
        private static void GenerarR3021D(Logger logger, BaseDatos baseDatos, string rutaSalida, string fecha_desde, string fecha_hasta)
        {
            try
            {
                //Marca y entidad debe armarlo en loop
                var lista_enti = baseDatos.GetListaEntidades();
                int marca = 8; //revisar para sacar la marca

                logger.Info("Estoy dentro del proceso R3021D");
                //for (int i = 0; i < lista_enti.Count(); i++)
                //{
                int entidad = 618;
                var nombreArchivo = ArmarNombreLP(marca.ToString(), entidad.ToString());
                var filas = baseDatos.GetAjustes(marca, entidad, fecha_desde, fecha_hasta);
                string envio = baseDatos.GetEnvio();
                if (filas.Count > 0)
                {
                    //Header
                    string ent = entidad.ToString().PadLeft(5, '0');
                    string mar = marca.ToString().PadLeft(3, '0');
                    string filler = "";
                    string descripcion = "Ajustes RG5330D";
                    string header = "1" + ent + mar + "R3021D  " + descripcion.PadRight(30, ' ') + envio.PadLeft(12, '0') + DateTime.Now.ToString("yyyyMMdd") + "00012" + filler.PadLeft(227, ' ');

                    using (var stream = new FileStream(rutaSalida + nombreArchivo, FileMode.Create, FileAccess.Write))
                    {
                        using (var writer = new StreamWriter(stream, Encoding.GetEncoding("ISO-8859-1"), 2 << 23))
                        {
                            decimal sumaimporpesos = 0;
                            decimal sumaimpordolar = 0;

                            logger.Info("Archivo de salida: " + nombreArchivo);
                            writer.AutoFlush = false;
                            writer.WriteLine(header);
                            foreach (var fila in filas)
                            {
                                writer.WriteLine(GenerarDetalle(fila, filler.PadLeft(204, ' ')));
                                //sumatorias
                            }

                            //Trailer
                            string canreg = filas.Count().ToString();
                            string trailer = "3" + ent + "R3021D  " + canreg.PadLeft(9, '0') + sumaimporpesos.ToString().PadLeft(16, '0') + sumaimpordolar.ToString().PadLeft(16, '0') + filler.PadLeft(244, ' ');

                            writer.WriteLine(trailer);
                        }
                        logger.Info("Archivo cerrado.");
                    }
                }
                //}
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                if (ex.InnerException != null)
                {
                    logger.Info(ex.InnerException.Message);
                }
            }
        }

        private static string GenerarDetalle(R3020D fila, string filler)
        {
            var linea = new StringBuilder(500);

            linea.Append(fila.tiporeg);
            linea.Append(fila.tipoope);
            linea.Append(fila.entidad_emisora.ToString().PadLeft(5, '0'));
            linea.Append(fila.sucursal_emisora.ToString().PadLeft(5, '0'));
            linea.Append(fila.entidad_cobradora.ToString().PadLeft(5, '0'));
            linea.Append(fila.sucursal_cobradora.ToString().PadLeft(5, '0'));
            linea.Append(fila.codigo_moneda.ToString().PadLeft(2, '0'));
            linea.Append(fila.comprobante.ToString().PadLeft(10, '0'));
            linea.Append(fila.fecha_movimiento.ToString("yyyyMMdd"));
            linea.Append(fila.numero_cuenta.ToString().PadLeft(10, '0'));
            string ret = ConvertValue(fila.importe_ajuste);
            linea.Append(ret.PadLeft(15, '0'));
            linea.Append(fila.concepto_ajuste.ToString().PadLeft(5, '0'));
            linea.Append(fila.deb_cred.ToString());
            linea.Append("001"); //ajustes sin cuotas

            //MEJORA 20/10/2023
            //linea.Append(fila.fecha_consumo.ToString("yyyyMMddHHmmss"));
            string consu = ConvertValue(fila.importe_consumo);
            linea.Append(consu.PadLeft(15, '0'));
            linea.Append(fila.mcc.ToString().PadLeft(5, '0'));
            linea.Append(fila.perso_docu_num_otro.ToString());
            linea.Append(filler);
            return linea.ToString();
        }

        private static List<ENTIPRODUCRG5330> GetListaEntiProduc()
        {
            List<ENTIPRODUCRG5330> lista = new List<ENTIPRODUCRG5330>();
            ENTIPRODUCRG5330 entprod = new ENTIPRODUCRG5330();
            string Entidad_Producto = ConfigurationManager.AppSettings["Entidad_Producto"];

            string[] strlist = Entidad_Producto.Split('|');
            foreach (var fila in strlist)
            {
                string[] dato = fila.Split('-');
                entprod.PAGAOTORENTICODI = Convert.ToInt32(dato[0]);
                entprod.PRODCTCODI = Convert.ToInt32(dato[1]);
                lista.Add(entprod);
            }

            return lista;
        }

        private static string GetEntidades(List<ENTIPRODUCRG5330> lista)
        {
            string entidades = string.Empty;
            foreach (var fila in lista)
            {
                var entidad = fila.PAGAOTORENTICODI;
                entidades += entidad.ToString() + ",";
            }

            entidades = entidades.TrimEnd(',');

            return entidades;
        }

        private static string GetProductos(List<ENTIPRODUCRG5330> lista)
        {
            string productos = string.Empty;
            foreach (var fila in lista)
            {
                var prod = fila.PRODCTCODI;
                productos += prod.ToString() + ",";
            }

            productos = productos.TrimEnd(',');

            return productos;
        }

        private static string ConvertValue(decimal valor)
        {
            decimal salida = 0;
            int e = 0;

            decimal data = Math.Round(valor, 2);
            if (!int.TryParse(valor.ToString(), out e))
            {
                // es decimal y no debo hacer nada
                salida = data;
            }
            else
            {
                salida = data * 100;
            }

            string retorno = salida.ToString();
            return retorno.Replace(",", "");
        }

        #endregion Reporte R3021
    }
}
